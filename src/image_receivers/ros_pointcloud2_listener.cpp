#include "ros_pointcloud2_listener.hpp"
#include <pcl_conversions/pcl_conversions.h>

namespace ImageReceivers
{
    template <typename PointT>
    void
    CloudTopic::ConvertPointClound2ToPCL(const sensor_msgs::PointCloud2 &pc, const typename boost::shared_ptr<pcl::PointCloud<PointT>> &cloud)
    {
        pcl::PCLPointCloud2 pcl_pc2;
        pcl_conversions::toPCL(pc, pcl_pc2);
        pcl::fromPCLPointCloud2(pcl_pc2, *cloud);
    }
    template void CloudTopic::ConvertPointClound2ToPCL<pcl::PointXYZRGB>(const sensor_msgs::PointCloud2 &pc, const typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> &cloud);
    template void CloudTopic::ConvertPointClound2ToPCL<pcl::PointXYZ>(const sensor_msgs::PointCloud2 &pc, const typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ>> &cloud);


    CloudTopic::CloudTopic(ros::NodeHandle * n, const std::string &topicName)
    {
        this->n = n;
        this->topicName = topicName;
    }

    void 
    CloudTopic::SubscribeToTopic()
    {
        sub = n->subscribe(topicName.c_str(), 1, &ImageReceivers::CloudTopic::CloudCallback, this);
        std::cout << "Subscribed to ImageTransport topic: " << topicName << std::endl;
    }

    void 
    CloudTopic::CloudCallback(const sensor_msgs::PointCloud2 &cloud_msg)
    {
        ROS_INFO("Image not null.");
        CloudTopic::ConvertPointClound2ToPCL<pcl::PointXYZRGB>(cloud_msg, cloudFromCallback);
        newCloud = true;
        ROS_INFO("exiting.");
    }

    bool
    CloudTopic::IsNewCloudReceived()
    {
        if (newCloud)
        {
            newCloud = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>>
    CloudTopic::GetCloud()
    {
        return cloudFromCallback;
    }
}