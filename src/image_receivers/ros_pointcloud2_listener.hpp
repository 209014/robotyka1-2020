#ifndef ROS_POINTCLOUD2_LISTENER_HPP
#define ROS_POINTCLOUD2_LISTENER_HPP

#include <pcl_conversions/pcl_conversions.h>

namespace ImageReceivers
{
    class CloudTopic
    {
    public:
        CloudTopic(ros::NodeHandle *n, const std::string &topicName);
        void SubscribeToTopic();
        void CloudCallback(const sensor_msgs::PointCloud2 &cloud_msg);
        bool IsNewCloudReceived();
        boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> GetCloud();

        template <typename PointT>
        static void
        ConvertPointClound2ToPCL(const sensor_msgs::PointCloud2 &pc, const typename boost::shared_ptr<pcl::PointCloud<PointT>> &cloud);

    private:
        bool newCloud = false;
        std::string topicName;
        ros::NodeHandle *n;
        ros::Subscriber sub;
        boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloudFromCallback = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGB>>();
    };
}


#endif
