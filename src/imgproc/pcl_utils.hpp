#ifndef PCL_UTILS_HPP
#define PCL_UTILS_HPP

#include <pcl/pcl_config.h>
#include <pcl/point_cloud.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/console/parse.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/statistical_outlier_removal.h>

namespace ImgProc
{
    template <typename PointT>
    double
    ComputeCloudResolution(const typename boost::shared_ptr<const pcl::PointCloud<PointT>> &cloud);


    boost::shared_ptr<pcl::visualization::PCLVisualizer>
    GetViewer(const std::string &viewerName);

    void
    AddCloudToViewer(const boost::shared_ptr<pcl::visualization::PCLVisualizer> &viewer, boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGB>> cloud, const std::string &cloudName);

    void
    AddCloudToViewer(const boost::shared_ptr<pcl::visualization::PCLVisualizer> &viewer, boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> cloud, const std::string &cloudName);

    void
    PrintModelCoefficients(const boost::shared_ptr<pcl::ModelCoefficients> &coefficients, int printFirstN);

    void
    PrintModelInliers(const boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> &cloud, const boost::shared_ptr<pcl::PointIndices> &inliers);

    template <typename PointT>
    pcl::PointCloud<PointT>
    LoadCloudPCDFile(const std::string &path);

    template <typename PointT>
    pcl::SACSegmentation<PointT>
    GetSegmentatorPlaneRANSAC(double distanceThreshold, int maxIterations);

    void
    GenerateSimpleColorShape(pcl::PointCloud<pcl::PointXYZRGB> *cloud);

    pcl::RGB 
    GetEasyDistinguishableColor(int i);
}

#endif
