#include "pcl_utils.hpp"

#include <pcl/pcl_config.h>
#include <pcl/point_cloud.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/console/parse.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/statistical_outlier_removal.h>

namespace ImgProc
{
    template <typename PointT>
    double
    ComputeCloudResolution(const typename boost::shared_ptr<const pcl::PointCloud<PointT>> &cloud)
    {
        double res = 0.0;
        int n_points = 0;
        int nres;
        std::vector<int> indices(2);
        std::vector<float> sqr_distances(2);
        pcl::search::KdTree<PointT> tree;
        tree.setInputCloud(cloud);

        for (std::size_t i = 0; i < cloud->size(); ++i)
        {
            if (!std::isfinite((*cloud)[i].x))
            {
                continue;
            }
            //Considering the second neighbor since the first is the point itself.
            nres = tree.nearestKSearch(i, 2, indices, sqr_distances);
            if (nres == 2)
            {
                res += sqrt(sqr_distances[1]);
                ++n_points;
            }
        }
        if (n_points != 0)
        {
            res /= n_points;
        }
        return res;
    }
    template double ComputeCloudResolution<pcl::PointXYZRGB>(const typename boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGB>> &cloud);
    template double ComputeCloudResolution<pcl::PointXYZ>(const typename boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> &cloud);


    boost::shared_ptr<pcl::visualization::PCLVisualizer>
    GetViewer(const std::string &viewerName)
    {
        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerRawCloud(new pcl::visualization::PCLVisualizer(viewerName));
        viewerRawCloud->setBackgroundColor(0, 0, 0);
        viewerRawCloud->addCoordinateSystem(1.0);
        viewerRawCloud->initCameraParameters();
        return (viewerRawCloud);
    }

    void
    AddCloudToViewer(const boost::shared_ptr<pcl::visualization::PCLVisualizer> &viewer, boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGB>> cloud, const std::string &cloudName)
    {
        pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
        viewer->addPointCloud<pcl::PointXYZRGB>(cloud, rgb, cloudName);
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, cloudName);
    }

    void
    AddCloudToViewer(const boost::shared_ptr<pcl::visualization::PCLVisualizer> &viewer, boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> cloud, const std::string &cloudName)
    {
        viewer->addPointCloud<pcl::PointXYZ>(cloud, cloudName);
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, cloudName);
    }

    void
    PrintModelCoefficients(const boost::shared_ptr<pcl::ModelCoefficients> &coefficients, int printFirstN)
    {
        int loopLimit = std::min(printFirstN, static_cast<int>(coefficients->values.size()));

        std::cout << "Model coefficients: ";
        for (size_t i = 0; i < loopLimit; i++)
        {
            std::cout << "  " << i << ": " << coefficients->values[i];
        }
        std::cout << std::endl;
    }

    void
    PrintModelInliers(const boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> &cloud, const boost::shared_ptr<pcl::PointIndices> &inliers)
    {
        std::cerr << "Model inliers: " << inliers->indices.size() << std::endl;
        for (std::size_t i = 0; i < inliers->indices.size(); ++i)
        {
            std::cerr << inliers->indices[i] << "    "
                      << cloud->points[inliers->indices[i]].x << " "
                      << cloud->points[inliers->indices[i]].y << " "
                      << cloud->points[inliers->indices[i]].z << std::endl;
        }
    }

    template <typename PointT>
    pcl::PointCloud<PointT>
    LoadCloudPCDFile(const std::string &path)
    {
        typename pcl::PointCloud<PointT> cloud_;
        if (pcl::io::loadPCDFile<PointT>(path, cloud_) == -1)
        {
            std::cerr << "Couldn't read file: " << path << std::endl;
            cloud_.clear();
        }
        else
        {
            std::cout << "File " << path << " loaded successfuly." << std::endl;
            std::cout << "Loaded " << (cloud_.width) * (cloud_.height) << " data points." << std::endl;
        }
        
        return cloud_;
    }
    template typename pcl::PointCloud<pcl::PointXYZRGB> LoadCloudPCDFile<pcl::PointXYZRGB>(const std::string &path);
    template typename pcl::PointCloud<pcl::PointXYZ> LoadCloudPCDFile<pcl::PointXYZ>(const std::string &path);

    template <typename PointT>
    pcl::SACSegmentation<PointT>
    GetSegmentatorPlaneRANSAC(double distanceThreshold, int maxIterations)
    {
        // Create the segmentation object
        pcl::SACSegmentation<PointT> seg;
        // Optional
        seg.setOptimizeCoefficients(true);
        // Mandatory
        seg.setModelType(pcl::SACMODEL_PLANE);
        seg.setMethodType(pcl::SAC_RANSAC);
        seg.setDistanceThreshold(0.0450);
        seg.setMaxIterations(50);

        return seg;
    }
    template typename pcl::SACSegmentation<pcl::PointXYZRGB> GetSegmentatorPlaneRANSAC<pcl::PointXYZRGB>(double distanceThreshold, int maxIterations);
    template typename pcl::SACSegmentation<pcl::PointXYZ> GetSegmentatorPlaneRANSAC<pcl::PointXYZ>(double distanceThreshold, int maxIterations);

    template <typename PointT>
    pcl::VoxelGrid<PointT>
    GetVoxelGrid(typename boost::shared_ptr<pcl::PointCloud<PointT>> cloudIn, float leafSizeCubic, pcl::PointCloud<PointT> &cloudOut)
    {
        pcl::VoxelGrid<PointT> vg;
        vg.setInputCloud(cloudIn);
        vg.setLeafSize(leafSizeCubic, leafSizeCubic, leafSizeCubic);
        vg.filter(cloudOut);

        return vg;
    }
    template typename pcl::VoxelGrid<pcl::PointXYZRGB> GetVoxelGrid<pcl::PointXYZRGB>(typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloudIn, float leafSizeCubic, pcl::PointCloud<pcl::PointXYZRGB> &cloudOut);
    template typename pcl::VoxelGrid<pcl::PointXYZ> GetVoxelGrid<pcl::PointXYZ>(typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ>> cloudIn, float leafSizeCubic, pcl::PointCloud<pcl::PointXYZ> &cloudOut);

    template <typename PointT>
    pcl::StatisticalOutlierRemoval<PointT>
    GetStatisticalOutlierFilter(typename boost::shared_ptr<pcl::PointCloud<PointT>> cloudIn, int meanK, double stdDevMult)
    {
        // Create the filtering object
        pcl::StatisticalOutlierRemoval<PointT> sor;
        sor.setInputCloud(cloudIn);
        sor.setMeanK(meanK);
        sor.setStddevMulThresh(stdDevMult);

        return sor;
    }
    template typename pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> GetStatisticalOutlierFilter<pcl::PointXYZRGB>(typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloudIn, int meanK, double stdDevMult);
    template typename pcl::StatisticalOutlierRemoval<pcl::PointXYZ> GetStatisticalOutlierFilter<pcl::PointXYZ>(typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ>> cloudIn, int meanK, double stdDevMult);


    void
    GenerateSimpleColorShape(pcl::PointCloud<pcl::PointXYZRGB> *cloud)
    {
        std::cout << "Generating example point clouds.\n\n";
        // We're going to make an ellipse extruded along the z-axis. The colour for
        // the XYZRGB cloud will gradually go from red to green to blue.
        std::uint8_t r(255), g(15), b(15);
        for (float z(-1.0); z <= 1.0; z += 0.05)
        {
            for (float angle(0.0); angle <= 360.0; angle += 5.0)
            {
                pcl::PointXYZRGB point;
                point.x = 0.5 * std::cos(pcl::deg2rad(angle));
                point.y = sinf(pcl::deg2rad(angle));
                point.z = z;
                std::uint32_t rgb = (static_cast<std::uint32_t>(r) << 16 |
                                     static_cast<std::uint32_t>(g) << 8 | static_cast<std::uint32_t>(b));
                point.rgb = *reinterpret_cast<float *>(&rgb);
                cloud->points.push_back(point);
            }
            if (z < 0.0)
            {
                r -= 12;
                g += 12;
            }
            else
            {
                g -= 12;
                b += 12;
            }
        }
    }

    pcl::RGB 
    GetEasyDistinguishableColor(int i)
    {
        static bool initialized = false;
        static pcl::RGB ergieb[7];
        if (!initialized)
        {
            ergieb[0].r = 255;
            ergieb[0].g = 0;
            ergieb[0].b = 0;

            ergieb[1].r = 0;
            ergieb[1].g = 255;
            ergieb[1].b = 0;

            ergieb[2].r = 0;
            ergieb[2].g = 0;
            ergieb[2].b = 255;

            ergieb[3].r = 255;
            ergieb[3].g = 255;
            ergieb[3].b = 0;

            ergieb[4].r = 255;
            ergieb[4].g = 0;
            ergieb[4].b = 255;

            ergieb[5].r = 0;
            ergieb[5].g = 255;
            ergieb[5].b = 255;

            ergieb[6].r = 255;
            ergieb[6].g = 255;
            ergieb[6].b = 255;

            initialized = true;
        }

        i = (i % (sizeof(ergieb)/sizeof(*ergieb)));

        return ergieb[i];
    }

    

    class PCLUtils
    {
    };
} // namespace ImgProc
