#include "picture_reader.hpp"
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <regex>
#include <boost/filesystem.hpp>
#include <boost/iterator/filter_iterator.hpp>
#include <boost/range/iterator_range.hpp>

#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

namespace fs = boost::filesystem;

namespace Recorders
{
    PictureReader::PictureReader(const std::string &directoryPath, const std::string &picturePrefix)
    {
        fileRGBPrefix = picturePrefix;
        pathToFiles = directoryPath;
        ReadDirectory();
    }

    int
    PictureReader::ReadDirectory(bool ignorePrefix)
    {
        fs::path p(pathToFiles.c_str());
        fs::directory_iterator dir_first(p), dir_last;

        imagePaths.clear();
        for(auto &entry : boost::make_iterator_range(fs::directory_iterator(p), {}))
        {
            std::string str = entry.path().string();
            int beginIdx = str.rfind('/');
            std::string filename = str.substr(beginIdx + 1);

            int extensionIdx = filename.find_last_of('.');
            std::string fileext = filename.substr(extensionIdx + 1);

            std::size_t found = filename.find(fileRGBPrefix);
            if (found != std::string::npos && fileext == "jpg")
            {
                imagePaths.push_back(str);
            }
        }
        
        std::sort(imagePaths.begin(), imagePaths.end());
        numberOfFiles = imagePaths.size();
        currentImage = 0;

        return numberOfFiles;
    }

    cv::Mat 
    PictureReader::GetImage(bool rollOver, bool forward)
    {
        currentImage += ((forward * 2) - 1);
        if (rollOver)
        {
            currentImage = currentImage % numberOfFiles;
            if(currentImage < 0)
                currentImage = (numberOfFiles - 1);
        }
        else
        {
            currentImage = std::min<int>(currentImage, (numberOfFiles - 1));
            currentImage = std::max<int>(currentImage, 0);
        }

        return cv::imread(imagePaths[currentImage], cv::ImreadModes::IMREAD_COLOR);        
    }
        
    void 
    PictureReader::JumpToFile(int number)
    {
        currentImage = number;
        currentImage = std::min<int>(currentImage, numberOfFiles);
        currentImage = std::max<int>(currentImage, 0);
    }

    void
    PictureReader::PrintImagePathList()
    {
        std::cout << "Loaded " << imagePaths.size() << " images." << std::endl;
        for (size_t i = 0; i < imagePaths.size(); i++)
        {
            std::cout << imagePaths[i] << std::endl;
        }
        
    }

    int
    PictureReader::GetImageCount()
    {
        return imagePaths.size();
    }

} // namespace Recorders
