#ifndef PICTURE_READER_HPP
#define PICTURE_READER_HPP

#include <string>
#include <vector>

#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

namespace Recorders
{
    class PictureReader
    {
    public:
        PictureReader(const std::string &directoryPath, const std::string &picturePrefix);
        int ReadDirectory(bool ignorePrefix = false);
        int GetImageCount();
        cv::Mat GetImage(bool rollOver = false, bool forward = true);
        void JumpToFile(int number);
        void PrintImagePathList();

    private:
        int numberOfFiles;
        int currentImage;
        std::string fileRGBPrefix;
        std::string pathToFiles;
        std::vector<std::string> imagePaths;
    };
}




#endif
