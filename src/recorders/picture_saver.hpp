#ifndef PICTURE_SAVER_HPP
#define PICTURE_SAVER_HPP

#include <string>
#include <opencv2/imgproc.hpp>

namespace Recorders
{
    class PictureSaver
    {
        //TODO: Check if directory is empty to use it and RemoveDirectory() inside Close().
    public:
        bool appendDateToPictureName;
        long maxNumberOfRecordedPictures;

        PictureSaver(const std::string &pictureNames, long maxNumberOfPictures, int saveOneInN);
        void SetPathToSaveDirectory(const std::string &path, const bool isRelativeToHome = true);
        void SetSaveDirectoryName(const std::string &name, bool useCurrentDatetime = true);
        bool PrepareSaveDirectory();
        void SaveTask(cv::Mat picture);
        void Close();
        void Start();
        void Stop();

        //TODO: Move outside class.
        static std::string CurrentDateTime(const bool compactFormat = false);
        static bool DirectoryExists(const std::string &path);
        static bool CreateDirectory(const std::string &path);
        static bool IsDirectoryEmpty(const std::string &path);
        static void RemoveDirectory(const std::string &path);

    private:
        bool saveDirectoryExists;
        bool recording;
        bool atLeastOneSaved;
        bool atLeastOnceTurnedOn;
        bool reachedMaxNumberOfRecordedPictures;
        int saveEveryNthPicture;
        int pictureSavePrescaler;
        long numberOfSavedPictured;
        long unsuccessfullySavedPictures;

        std::string pathToSaveDirectory;
        std::string saveDirectoryName;
        std::string fullAbsolutePath;
        std::string pictureNames;

        void InitDefaults();
    };
} // namespace Recorders

#endif
