#include <string>
#include <chrono>
#include <sys/stat.h>
#include <sys/types.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

#include "picture_saver.hpp"

#include <opencv2/core.hpp>
#include <iostream>


using namespace std;
using namespace cv;

namespace Recorders
{
    PictureSaver::PictureSaver(const std::string &pictureNames, long maxNumberOfPictures, int saveOneInN)
    {
        this->pictureNames = pictureNames;
        this->maxNumberOfRecordedPictures = maxNumberOfPictures;
        this->saveEveryNthPicture = saveOneInN;

        InitDefaults();
        SetSaveDirectoryName("");
        SetPathToSaveDirectory("/uav_recorder");
    }

    void
    PictureSaver::InitDefaults()
    {
        this->appendDateToPictureName = false;
        this->saveDirectoryExists = false;
        this->recording = false;
        this->atLeastOneSaved = false;
        this->atLeastOnceTurnedOn = false;
        this->reachedMaxNumberOfRecordedPictures = false;
        this->numberOfSavedPictured = 0;
        this->unsuccessfullySavedPictures = 0;
        this->pictureSavePrescaler = 0;
    }

    void
    PictureSaver::SetSaveDirectoryName(const std::string &name, bool appendCurrentDatetime)
    {
        saveDirectoryName = "/" + name;
        if (appendCurrentDatetime)
            saveDirectoryName += CurrentDateTime();
    }

    std::string
    PictureSaver::CurrentDateTime(const bool compactFormat)
    {
        std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

        //Too long on purpose. I don't want any problems with this ever.
        char buf[128] = {0};

        if (!compactFormat)
            std::strftime(buf, sizeof(buf), "Date_%Y-%m-%d_Time_%H-%M-%S", std::localtime(&now));
        else
            std::strftime(buf, sizeof(buf), "D%Y%m%dT%H%M%S", std::localtime(&now));

        return buf;
    }

    void
    PictureSaver::SetPathToSaveDirectory(const std::string &path, const bool isRelativeToHome)
    {
        if (isRelativeToHome)
            pathToSaveDirectory = getenv("HOME") + path;
        else
            pathToSaveDirectory = path;
    }

    void
    PictureSaver::Start()
    {
        recording = true;
        atLeastOnceTurnedOn = true;
    }

    void
    PictureSaver::Stop()
    {
        recording = false;
    }

    bool
    PictureSaver::PrepareSaveDirectory()
    {
        if (recording)
        {
            std::cerr << "Path cannot be changed while running." << std::endl;
            return false;
        }

        if (atLeastOnceTurnedOn)
        {
            Close();
            atLeastOnceTurnedOn = false;
        }

        fullAbsolutePath = pathToSaveDirectory + saveDirectoryName;
        if (!DirectoryExists(fullAbsolutePath))
        {
            return CreateDirectory(fullAbsolutePath);
        }
        return true;
    }

    bool
    PictureSaver::DirectoryExists(const std::string &path)
    {
        struct stat buffer;
        return (stat(path.c_str(), &buffer) == 0);
    }

    bool
    PictureSaver::CreateDirectory(const std::string &path)
    {
        //Too long on purpose. I don't want any problems with this ever.
        char commandPathBuffer[512];
        sprintf(commandPathBuffer, "mkdir -p %s", path.c_str());
        int status = system(commandPathBuffer);

        if (status == -1)
        {
            std::cerr << "Error : " << strerror(errno) << std::endl;
            return false;
        }
        else
        {
            std::cout << "Directories are created" << std::endl;
            return true;
        }
    }

    void
    PictureSaver::SaveTask(cv::Mat picture)
    {
        // cv::Mat noise(picture.size(),picture.type());
        // float m = (5,6,17);
        // float sigma = (1,5,50);
        // cv::randn(noise, m, sigma);
        // picture += noise;

        // picture.convertTo(picture, CV_8UC1, 0.03125);

        // cv::Mat in[] = {picture, picture, picture};
        // cv::merge(in, 3, picture);

        // cv::Mat temp(picture.size(),picture.type());
        
        // cv::Mat Kernel = cv::Mat(cv::Size(3,3),CV_8UC1,cv::Scalar(255));
        // cv::Mat Kernel2 = cv::Mat(cv::Size(5,5),CV_8UC1,cv::Scalar(255));
        // // cv::Mat Kernel3 = cv::Mat(cv::Size(7,7),CV_8UC1,cv::Scalar(255));
        
        // cv::morphologyEx(picture,temp,cv::MORPH_OPEN,Kernel);
        // cv::morphologyEx(temp,picture,cv::MORPH_CLOSE,Kernel);
        // cv::morphologyEx(picture,temp,cv::MORPH_OPEN,Kernel2);
        // cv::morphologyEx(temp,picture,cv::MORPH_CLOSE,Kernel2);
        // // cv::morphologyEx(picture,temp,cv::MORPH_OPEN,Kernel3);
        // // cv::morphologyEx(temp,picture,cv::MORPH_CLOSE,Kernel3);

        // cv::pyrMeanShiftFiltering(picture,temp,5,5,1);

        // picture=temp;

        // cv::Canny(picture,temp,40,140,3,false);

        // picture=temp;

        // vector<Vec2f> lines;

        // cv::imshow("picture", picture);

        // cv::HoughLines(picture,lines, 1, 0.157, 60, 0, 0 );

        // cv::cvtColor(picture, picture,cv::COLOR_GRAY2BGR);

        // for( size_t i = 0; i < lines.size(); i++ )
        // {
        //     float rho = lines[i][0];
        //     float theta = lines[i][1];

        //     std::cout << "rho" << rho <<std::endl;
        //     std::cout << "theta" << theta <<std::endl;
        //     Point pt1, pt2;
        //     double a = cos(theta), b = sin(theta);
        //     double x0 = a*rho, y0 = b*rho;
        //     pt1.x = cvRound(x0 + 1000*(-b));
        //     pt1.y = cvRound(y0 + 1000*(a));
        //     pt2.x = cvRound(x0 - 1000*(-b));
        //     pt2.y = cvRound(y0 - 1000*(a));
        //     std::cout << "pkt1" << pt1 <<std::endl;
        //     std::cout << "pkt2" << pt2 <<std::endl;
        //     cv::line(picture, pt1, pt2, Scalar(0,0,255), 3, LINE_AA);
        // }

        // cv::imshow("picture2", picture);

        // vector<Vec2f> corners;

        // cv::goodFeaturesToTrack(picture,corners, 20, 0.5, 50);

        // cv::cvtColor(picture, picture,cv::COLOR_GRAY2BGR);

        // for( size_t i = 0; i < corners.size(); i++ )
        // {
        //     Point pt;
        //     pt.x = corners[i][0];
        //     pt.y = corners[i][1];
        //     std::cout << "pkt" << pt <<std::endl;
        //     cv::circle(picture, pt, 2, Scalar(0,0,255), 2);
        // }

        // cv::imshow("picture2", picture);

        if (!recording)
        {
            return;
        }
        else if (numberOfSavedPictured >= maxNumberOfRecordedPictures)
        {
            reachedMaxNumberOfRecordedPictures = true;
            return;
        }
        else if (pictureSavePrescaler == 0)
        {
            //Too long on purpose. I don't want any problems with this ever.
            char picturePath[512];

            if (appendDateToPictureName)
                sprintf(picturePath, "%s/%s_%05ld_%s_.bmp", fullAbsolutePath.c_str(), pictureNames.c_str(), numberOfSavedPictured, CurrentDateTime(true).c_str());
            else
                sprintf(picturePath, "%s/%s_%05ld.bmp", fullAbsolutePath.c_str(), pictureNames.c_str(), numberOfSavedPictured);

            bool success = imwrite(picturePath, picture);
            if (success)
            {
                atLeastOneSaved = true;
                numberOfSavedPictured++;
            }
            else
            {
                unsuccessfullySavedPictures++;
            }
        }
        pictureSavePrescaler = ((pictureSavePrescaler + 1) % saveEveryNthPicture);
    }

    void
    PictureSaver::Close()
    {
        Stop();

        if (atLeastOneSaved)
        {
            std::cout << pictureNames << " recorder: Saved " << numberOfSavedPictured << " pictures in: " << fullAbsolutePath << std::endl;
        }
        else
        {
            if(atLeastOnceTurnedOn)
            {
                std::string msg = pictureNames + " recorder: ATTENTION! No images have been saved despite recorder beeing turned on: ";
                std::cerr << msg << fullAbsolutePath << std::endl;
            }
            else
            {
                std::string msg = pictureNames + " recorder: Recorder was turned off: ";
                std::cout << msg << fullAbsolutePath << std::endl;
            }
            
            if(IsDirectoryEmpty(fullAbsolutePath))
                RemoveDirectory(fullAbsolutePath);
        }
    }

    bool
    PictureSaver::IsDirectoryEmpty(const std::string &path)
    {
        //TODO: Implement.
        return false;
    }

    void
    PictureSaver::RemoveDirectory(const std::string &path)
    {
        //Too long on purpose. I don't want any problems with this ever.
        char commandPathBuffer[512];
        sprintf(commandPathBuffer, "rmdir %s", path.c_str());
        int status = system(commandPathBuffer); // Creating a directory

        if (status == -1)
            std::cerr << "Error : " << strerror(errno) << std::endl;
        else
            std::cout << "Folder removed." << std::endl;
    }
} // namespace Recorders
