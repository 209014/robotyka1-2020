#ifndef PROGRAM_ARGUMENTS_HPP
#define PROGRAM_ARGUMENTS_HPP

#include <string>
#include <opencv2/opencv.hpp>

namespace AppOptions
{
    struct ProgramArguments
    {
        bool printHelpMessage = false;

        bool recordAllFramesToFiles = false;

        bool recordRosRGBFramesToFiles = false;
        bool recordRosDepthFramesToFiles = false;
        bool recordRosPC2FramesToFiles = false;
        bool recordCameraFramesToFiles = false;

        int recordEveryNthFrame = 5;

        bool retrievePoincloud2Rostopic = false;
        std::string rostopicPoincloud2Name;

        bool retrieveRGBFromRostopic = false;
        std::string rostopicRGBName;

        bool retrieveDepthImageFromRostopic = false;
        std::string rostopicDepthImageName;

        bool retrieveFromCamera = false;
        int cameraID = 0;
        cv::Size cameraFrameSize = cv::Size(960, 720);
        bool widthOrHeightWasSet = false;

        std::string imagePath;
        bool imageRead = false;
    };
} // namespace AppOptions
#endif /* PROGRAM_ARGUMENTS_HPP */
