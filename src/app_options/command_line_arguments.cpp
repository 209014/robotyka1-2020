#include "command_line_arguments.hpp"
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "json.hpp"

namespace po = boost::program_options;

namespace AppOptions
{
    CmdArgumentsReader::CmdArgumentsReader(int argc, char **argv)
    {
        optionsDescription = std::unique_ptr<po::options_description>(new po::options_description("Allowed options"));
        argumentMap = std::unique_ptr<po::variables_map>(new po::variables_map());

        PopulateOptionsDescription();

        try
        {
            po::store(po::parse_command_line(argc, argv, *optionsDescription), *argumentMap);
        }
        catch (po::error const &e)
        {
            std::cerr << e.what() << ". Type '--help' for more info." << std::endl
                      << std::endl;
            unrecognizedCommandFound = true;
        }
    }

    void
    CmdArgumentsReader::PopulateOptionsDescription()
    {
        optionsDescription->add_options()("help", "produce help message")
        ("from-topic-rgb", po::value<std::string>(), "set ros topic RGB image name")
        ("from-topic-depth", po::value<std::string>(), "set ros topic depth image name")
        ("point-cloud2", po::value<std::string>(), "set ros topic pointCloud2 image name")
        ("cam-id", po::value<int>(), "set camera id for opencv connection")
        ("width", po::value<int>(), "set camera frame width")
        ("height", po::value<int>(), "set camera frame height")
        ("record-all", po::value<bool>(), "Turn on recording of all active sources")
        ("record-rgb", po::value<bool>(), "Turn on recording of RGB ros image topic")
        ("record-depth", po::value<bool>(), "Turn on recording of depth ros image topic")
        ("record-pc2", po::value<bool>(), "UNIMPLEMENTED!. Turn on recording of pointcloud2 ros topic")
        ("record-cam", po::value<bool>(), "Turn on recording of camera")
        ("boostecho", po::value<std::string>(), "test boost behaviour")
        ("record-interval", po::value<int>(), "set frame grab interval. Get one every <interval> frame. The same for all record sources.")
        ("project", po::value<std::string>(), "project.");
    }

    bool
    CmdArgumentsReader::IsHelpOrUnrecognizedCommandPresent()
    {
        return ((argumentMap->find("help") != argumentMap->end()) || unrecognizedCommandFound);
    }

    void
    CmdArgumentsReader::PrintHelpMessage()
    {
        std::cout << *optionsDescription << "\n";
    }

    void
    CmdArgumentsReader::Parse(ProgramArguments *programArguments)
    {
        po::notify(*argumentMap);

        PopulateOptionsValues(programArguments);
    }

    void
    CmdArgumentsReader::PopulateOptionsValues(ProgramArguments *programArguments)
    {
        if (argumentMap->find("boostecho") != argumentMap->end())
        {
            std::cout << "boostecho argument used."
                      << "\n";
        }

        if (argumentMap->find("width") != argumentMap->end())
        {
            programArguments->cameraFrameSize.width = (*argumentMap)["width"].as<int>();
            programArguments->widthOrHeightWasSet = true;
        }

        if (argumentMap->find("height") != argumentMap->end())
        {
            programArguments->cameraFrameSize.height = (*argumentMap)["height"].as<int>();
            programArguments->widthOrHeightWasSet = true;
        }

        if (programArguments->widthOrHeightWasSet)
        {
            std::cout << "Frame size: " << programArguments->cameraFrameSize << std::endl;
        }

        if (argumentMap->find("record-all") != argumentMap->end())
        {
            programArguments->recordAllFramesToFiles = (*argumentMap)["record-all"].as<bool>();
        }

        if (argumentMap->find("record-rgb") != argumentMap->end())
        {
            programArguments->recordRosRGBFramesToFiles = (*argumentMap)["record-rgb"].as<bool>();
        }

        if (argumentMap->find("record-depth") != argumentMap->end())
        {
            programArguments->recordRosDepthFramesToFiles = (*argumentMap)["record-depth"].as<bool>();
        }

        if (argumentMap->find("record-cam") != argumentMap->end())
        {
            programArguments->recordCameraFramesToFiles = (*argumentMap)["record-cam"].as<bool>();
        }

        if (argumentMap->find("record-interval") != argumentMap->end())
        {
            int recordInterval = (*argumentMap)["record-interval"].as<int>();
            if (recordInterval >= 1)
                programArguments->recordEveryNthFrame = recordInterval;
            else
                std::cout << "Incorrect interval. Correct values are >= 1. Assuming default." << std::endl;
        }

        if (argumentMap->find("from-topic-rgb") != argumentMap->end())
        {
            programArguments->retrieveRGBFromRostopic = true;
            programArguments->rostopicRGBName = (*argumentMap)["from-topic-rgb"].as<std::string>();
            std::cout << "ROS RGB image topic name: " << programArguments->rostopicRGBName << std::endl;
        }

        if (argumentMap->find("from-topic-depth") != argumentMap->end())
        {
            programArguments->retrieveDepthImageFromRostopic = true;
            programArguments->rostopicDepthImageName = (*argumentMap)["from-topic-depth"].as<std::string>();
            std::cout << "ROS RGB image topic name: " << programArguments->rostopicDepthImageName << std::endl;
        }

        if (argumentMap->find("point-cloud2") != argumentMap->end())
        {
            programArguments->retrievePoincloud2Rostopic = true;
            programArguments->rostopicPoincloud2Name = (*argumentMap)["point-cloud2"].as<std::string>();
            std::cout << "ROS pointcloud2 image topic name: " << programArguments->rostopicDepthImageName << std::endl;
        }

        if (argumentMap->find("cam-id") != argumentMap->end())
        {
            programArguments->retrieveFromCamera = true;
            programArguments->cameraID = (*argumentMap)["cam-id"].as<int>();
        }
        if (argumentMap->find("project") != argumentMap->end())
        {
            programArguments->imagePath = (*argumentMap)["project"].as<std::string>();
            programArguments->imageRead = true;
        }
    }

    void
    NloohTest()
    {
        // create an empty structure (null)
        nlohmann::json j;

        // add a number that is stored as double (note the implicit conversion of j to an object)
        j["pi"] = 3.141;

        // add a Boolean that is stored as bool
        j["happy"] = true;

        // add a string that is stored as std::string
        j["name"] = "Niels";

        // add another null object by passing nullptr
        j["nothing"] = nullptr;

        // add an object inside the object
        j["answer"]["everything"] = 42;

        // add an array that is stored as std::vector (using an initializer list)
        j["list"] = {1, 0, 2};

        // add another object (using an initializer list of pairs)
        j["object"] = {{"currency", "USD"}, {"value", 42.99}};

        // instead, you could also write (which looks very similar to the JSON above)
        nlohmann::json j2 = {
            {"pi", 3.141},
            {"happy", true},
            {"name", "Niels"},
            {"nothing", nullptr},
            {"answer", {{"everything", 42}}},
            {"list", {1, 0, 2}},
            {"object", {{"currency", "USD"}, {"value", 42.99}}}};

        // write prettified JSON to another file
        std::ofstream p("pretty2.json");
        p << std::setw(4) << j2 << std::endl;

        std::ifstream i("pretty1.json");
        nlohmann::json js;
        i >> js;
        if (js.find("test") != js.end())
        {
            std::cout << "Jest test" << std::endl;
            std::cout << "test ma wartosc " << js["test"].get<int>() << std::endl;
        }
    }

} // namespace AppOptions
