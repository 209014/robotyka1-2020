#include "ros/ros.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <bits/stdc++.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>

// Point cloud library
#include <pcl/pcl_config.h>
#include <pcl/point_cloud.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/console/parse.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include "app_options/program_arguments.hpp"
#include "app_options/command_line_arguments.hpp"
#include "recorders/picture_saver.hpp"
#include "recorders/picture_reader.hpp"
#include "imgproc/pcl_utils.hpp"
#include "image_receivers/ros_image2D_listener.hpp"
#include "image_receivers/ros_pointcloud2_listener.hpp"

void Invitation(void);
int ConnectToCamera(int cameraID, cv::Size *frameSize, cv::VideoCapture *cap);
template <typename PointT>
int PrintSegmentInfo(const typename boost::shared_ptr<const pcl::PointCloud<PointT>> cloud, bool computeDensity);
boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> SegmentPlanes(pcl::SACSegmentation<pcl::PointXYZRGB> &seg, pcl::PointCloud<pcl::PointXYZRGB> &cloud);

int main(int argc, char **argv)
{
    Invitation();

    AppOptions::CmdArgumentsReader cmdArgsReader(argc, argv);

    if (cmdArgsReader.IsHelpOrUnrecognizedCommandPresent())
    {
        cmdArgsReader.PrintHelpMessage();
        return 0;
    }

    AppOptions::ProgramArguments programArgs;
    cmdArgsReader.Parse(&programArgs);

    /////////////////////////////////////////////////////////////////////////////////////////////////

    boost::shared_ptr<Recorders::PictureSaver> camPictureRecorder = boost::make_shared<Recorders::PictureSaver>("cam", 45000, programArgs.recordEveryNthFrame);
    boost::shared_ptr<Recorders::PictureSaver> rosRGBPictureRecorder = boost::make_shared<Recorders::PictureSaver>("rosRGB", 45000, programArgs.recordEveryNthFrame);
    boost::shared_ptr<Recorders::PictureSaver> rosDepthPictureRecorder = boost::make_shared<Recorders::PictureSaver>("rosDepth", 45000, programArgs.recordEveryNthFrame);

    if (programArgs.recordAllFramesToFiles)
    {
        programArgs.recordCameraFramesToFiles = true;
        programArgs.recordRosRGBFramesToFiles = true;
        programArgs.recordRosDepthFramesToFiles = true;
    }

    if (programArgs.recordCameraFramesToFiles)
    {
        camPictureRecorder->PrepareSaveDirectory();
        camPictureRecorder->Start();
    }

    if (programArgs.recordRosRGBFramesToFiles)
    {
        rosRGBPictureRecorder->PrepareSaveDirectory();
        rosRGBPictureRecorder->Start();
    }

    if (programArgs.recordRosDepthFramesToFiles)
    {
        rosDepthPictureRecorder->PrepareSaveDirectory();
        rosDepthPictureRecorder->Start();
    }

    if (programArgs.imageRead)
    {
        cv::Mat color, depth;
        std::string pathColor, pathDepth;

        pathColor.append(programArgs.imagePath);
        pathDepth.append(programArgs.imagePath);
        pathColor.append("color.bmp");
        pathDepth.append("depth.bmp");

        color = cv::imread(pathColor, cv::IMREAD_COLOR);
        depth = cv::imread(pathDepth, cv::IMREAD_COLOR);

        cv::imwrite("/home/tymoteusz/zapis/0)originalColor.bmp",color);
        cv::imwrite("/home/tymoteusz/zapis/0)originalDepth.bmp",depth);

    // PSUCIE  TYLKO NA SYMULACJI!!
        cv::Mat noiseColor(color.size(),color.type());
        cv::Mat noiseDepth(depth.size(),depth.type());
        float m = (5,6,17);
        float sigma = (1,5,50);
        cv::randn(noiseColor, m, sigma);
        cv::randn(noiseDepth, m, sigma);
        color += noiseColor;
        depth += noiseDepth;

        cv::imwrite("/home/tymoteusz/zapis/1)noisyColor.bmp",color);
        cv::imwrite("/home/tymoteusz/zapis/1)noisyDepth.bmp",depth);

    // NAPRAWIANIE

        cv::Mat depthZ(depth.size(),depth.type());

        cv::Mat tempColor(color.size(),color.type());
        cv::Mat tempDepth(depth.size(),depth.type());

        cv::Mat Kernel = cv::Mat(cv::Size(3,3),CV_8UC1,cv::Scalar(255));
        cv::Mat Kernel2 = cv::Mat(cv::Size(5,5),CV_8UC1,cv::Scalar(255));
        cv::morphologyEx(color,tempColor,cv::MORPH_OPEN,Kernel);
        cv::morphologyEx(tempColor,color,cv::MORPH_CLOSE,Kernel);
        cv::morphologyEx(color,tempColor,cv::MORPH_OPEN,Kernel2);
        cv::morphologyEx(tempColor,color,cv::MORPH_CLOSE,Kernel2);
        cv::morphologyEx(depth,tempDepth,cv::MORPH_OPEN,Kernel);
        cv::morphologyEx(tempDepth,depth,cv::MORPH_CLOSE,Kernel);
        cv::morphologyEx(depth,tempDepth,cv::MORPH_OPEN,Kernel2);
        cv::morphologyEx(tempDepth,depth,cv::MORPH_CLOSE,Kernel2);

        depthZ=depth;

        cv::imwrite("/home/tymoteusz/zapis/2)fixedColor.bmp",color);
        cv::imwrite("/home/tymoteusz/zapis/2)fixedDepth.bmp",depth);

    // DZIURY  TYLKO NA RZECZYWISTYM!!

        // cv::Mat red(depth.size(),CV_8UC3);

        // cv::threshold(depth, tempDepth, 0, 255, cv::THRESH_BINARY_INV);

        // red = cv::Scalar(0,0,255);

        // cv::bitwise_and(tempDepth, red, tempDepth);

        // cv::Mat mask;

        // mask=tempDepth;

        // cv::cvtColor(mask,mask,cv::COLOR_BGR2GRAY);

        // cv::threshold(mask, mask, 10, 255, cv::THRESH_BINARY);

        // cv::inpaint(depth, mask, depth, 5, cv::INPAINT_NS);

        // cv::imshow("mask", mask);
        // cv::imshow("depthMask",tempDepth);

        // depthZ=depth;

        // cv::imwrite("/home/tymoteusz/zapis/2_2)fixedDepth.bmp",depth);

    // MEAN SHIFT + CANNY

        cv::pyrMeanShiftFiltering(color,tempColor,5,5,1);
        cv::pyrMeanShiftFiltering(depth,tempDepth,5,5,1);

        color=tempColor;
        depth=tempDepth;

        cv::Canny(color,tempColor,40,140,3,false); //symulacja
        cv::Canny(depth,tempDepth,40,140,3,false);

        //cv::Canny(color,tempColor,100,200,3,false); //rzeczywisty
        //cv::Canny(depth,tempDepth,20,50,3,false);

        color=tempColor;
        depth=tempDepth;

        cv::Mat colorCanny=color;

        cv::imwrite("/home/tymoteusz/zapis/3)cannyColor.bmp",color);
        cv::imwrite("/home/tymoteusz/zapis/3)cannyDepth.bmp",depth);

    // COLOR + DEPTH
        
        cv::Mat together;

        cv::Rect myROI(158, 90, 962, 538); //symulacja
        //cv::Rect myROI(111, 84, 415, 311); //rzeczywisty

        depth = depth(myROI);
        depthZ = depthZ(myROI);

        cv::resize(color,color,depth.size());
        together=color+depth;

        cv::threshold(together, together, 10, 255, cv::THRESH_BINARY);

        cv::imwrite("/home/tymoteusz/zapis/4)together.bmp",together);
        cv::imwrite("/home/tymoteusz/zapis/4_1)notTogether.bmp",color);

    // POINTS

        cv::Mat togetherCorners;
        std::vector<cv::Vec2f> corners;

        //cv::Rect cornerROI(119, 89, 239, 179);
        //colorCanny=colorCanny(cornerROI);

        cv::goodFeaturesToTrack(together,corners, 50, 0.2, 50); //symulacja
        //cv::goodFeaturesToTrack(color,corners, 50, 0.2, 50); //rzeczywisty

        cv::cvtColor(together, togetherCorners,cv::COLOR_GRAY2BGR); //symulacja
        //cv::cvtColor(color, togetherCorners,cv::COLOR_GRAY2BGR); //rzeczywisty

        ofstream myfile;
        myfile.open ("/home/tymoteusz/zapis/Points.txt");

        for( size_t i = 0; i < corners.size(); i++ )
        {
            cv::Point pt;
            int z;
            pt.x = corners[i][0];
            pt.y = corners[i][1];
            z=depthZ.at<cv::Vec3b>(pt.y,pt.x)[1];
            myfile << "[x;y]=[" << pt.x << ";" << pt.y << ";" << z << "]\n";
            cv::circle(togetherCorners, pt, 2, cv::Scalar(0,0,255), 2);
        }

        myfile.close();

        cv::imwrite("/home/tymoteusz/zapis/5)togetherCorners.bmp",togetherCorners);

    // LINES

        // cv::Mat togetherLines;
        cv::Mat togetherProb;
        // std::vector<cv::Vec2f> lines;
        std::vector<cv::Vec4i> linesProb;

        //cv::HoughLines(together,lines, 1, 0.0157, 100, 0, 0 ); //symulacja
        //cv::HoughLines(together,lines, 1, 0.0157, 120, 0, 0 ); //rzeczywisty

        cv::HoughLinesP(together,linesProb, 2, CV_PI/360, 50, 25, 15); //symulacja
        //cv::HoughLinesP(color,linesProb, 2, CV_PI/360, 50, 25, 15); //rzeczywisty

        // cv::cvtColor(together, togetherLines,cv::COLOR_GRAY2BGR);

        cv::cvtColor(together, togetherProb,cv::COLOR_GRAY2BGR); //symulacja
        //cv::cvtColor(color, togetherProb,cv::COLOR_GRAY2BGR); //rzeczywisty

        // for( size_t i = 0; i < lines.size(); i++ )
        // {
        //     float rho = lines[i][0];
        //     float theta = lines[i][1];
        //     int zx, zy;
        //     cv::Point pt1, pt2;
        //     double a = cos(theta), b = sin(theta);
        //     double x0 = a*rho, y0 = b*rho;
        //     pt1.x = cvRound(x0 + 1000*(-b));
        //     pt1.y = cvRound(y0 + 1000*(a));
        //     pt2.x = cvRound(x0 - 1000*(-b));
        //     pt2.y = cvRound(y0 - 1000*(a));

        //     std::cout << "pt1" << pt1 <<std::endl;
        //     std::cout << "pt2" << pt2 <<std::endl;

        //     if(pt1.x>950||pt1.x<-950){
        //         zy=(pt1.y+pt2.y)/2;
        //         std::cout << "zy" << zy <<std::endl;           
        //     }
        //     else if(pt1.y>950||pt1.y<-950){
        //         zx=(pt1.x+pt2.x)/2;
        //         std::cout << "zx" << zx <<std::endl;
        //     }
            
        //     cv::line(togetherLines, pt1, pt2, cv::Scalar(0,0,255), 3, cv::LINE_AA);
        // }

        ofstream file;
        file.open ("/home/tymoteusz/zapis/Lines.txt");

        for( size_t i = 0; i < linesProb.size(); i++ )
        {
            cv::Vec4i l = linesProb[i];
            int zs, ze;
            zs=depthZ.at<cv::Vec3b>(l[1],l[0])[1];
            ze=depthZ.at<cv::Vec3b>(l[3],l[2])[1];
            file << "Start=[" << l[0] << ";" << l[1] << ";" << zs << "] End=[" << l[2] << ";" << l[3] << ";" << ze << "]\n";
            cv::line( togetherProb, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(0,0,255), 3, cv::LINE_AA);
        }
        file.close();

        // cv::Mat togetherLines2;

        // cv::cvtColor(together, togetherLines2,cv::COLOR_GRAY2BGR);

        // cv::bitwise_and(togetherLines2, togetherLines, tempDepth);

        // cv::imwrite("/home/tymoteusz/zapis/6)togetherLines.bmp",togetherLines);

        cv::imwrite("/home/tymoteusz/zapis/6_3)togetherProb.bmp",togetherProb);

        // cv::imwrite("/home/tymoteusz/zapis/6_2)togetherLines2.bmp",tempDepth);

    // COŚ

    // int ii=tempDepth.rows;
    // int jj=tempDepth.cols;
    // int i,j;

    // for (j=0; j<jj; j++){
        
    //     for (i=0; i<ii; i++){
            
    //         if (tempDepth.at<cv::Vec3b>(i,j)==cv::Vec3b(0,0,255)){
                
    //             tempDepth.at<cv::Vec3b>(i,j)=cv::Vec3b(0,255,0);
    //         }
    //     }
        


    // }

    // cv::imshow("something",tempDepth);







    // CIRCLES

        // cv::Mat togetherCircles;
        // std::vector<cv::Vec3f> circles;

        // //cv::HoughCircles(together,circles, cv::HOUGH_GRADIENT, 2, together.rows/4, 200, 100); //symulacja
        // cv::HoughCircles(together,circles, cv::HOUGH_GRADIENT, 50, together.rows/4, 200, 100); //rzeczywisty

        // cv::cvtColor(together, togetherCircles,cv::COLOR_GRAY2BGR);

        // for( size_t i = 0; i < circles.size(); i++ )
        // {
        //     int rad = circles[i][2];
        //     cv::Point ptc;
        //     ptc.x = circles[i][0];
        //     ptc.y = circles[i][1];
        //     cv::circle(togetherCircles, ptc, rad, cv::Scalar(0,0,255), 3);
        // }

        // cv::imwrite("/home/tymoteusz/zapis/7)togetherCircles.bmp",togetherCircles);


    }

    ros::init(argc, argv, "ProjectBase");
    ros::NodeHandle n;
    ImageReceivers::Image2DTopic imageTopicRGB(n, programArgs.rostopicRGBName);
    ImageReceivers::Image2DTopic imageTopicDepth(n, programArgs.rostopicDepthImageName);
    ImageReceivers::CloudTopic cloudTopic(&n, programArgs.rostopicPoincloud2Name);

    cv::VideoCapture cap;

    if (programArgs.retrieveRGBFromRostopic)
        imageTopicRGB.SubscribeToTopic();
    if (programArgs.retrieveDepthImageFromRostopic)
        imageTopicDepth.SubscribeToTopic();
    if (programArgs.retrievePoincloud2Rostopic)
        cloudTopic.SubscribeToTopic();
    if (programArgs.retrieveFromCamera)
    {
        if (!ConnectToCamera(programArgs.cameraID, &programArgs.cameraFrameSize, &cap))
        {
            std::cerr << "Camera capture connection failed." << std::endl;
            programArgs.retrieveFromCamera = false;
        }
    }

    const boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> segmentedColourizedCloud(new pcl::PointCloud<pcl::PointXYZRGB>);

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerRawCloud = ImgProc::GetViewer("3D rgbVis");
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerSegPlanes = ImgProc::GetViewer("3D rgb planar Vis");
    ImgProc::AddCloudToViewer(viewerRawCloud, cloudTopic.GetCloud(), "sample cloud");
    ImgProc::AddCloudToViewer(viewerSegPlanes, segmentedColourizedCloud, "sample cloud");

    pcl::SACSegmentation<pcl::PointXYZRGB> seg = ImgProc::GetSegmentatorPlaneRANSAC<pcl::PointXYZRGB>(0.135, 50);

    while (ros::ok())
    {

        if (programArgs.retrieveFromCamera)
        {
            cv::Mat frameIN;
            if (cap.read(frameIN))
            {
                cv::imshow("webcam", frameIN);
                camPictureRecorder->SaveTask(frameIN);
            }
        }

        if (programArgs.retrievePoincloud2Rostopic)
        {
            if (cloudTopic.IsNewCloudReceived())
            {
                boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> colourizedSegmentedCloud = SegmentPlanes(seg, *cloudTopic.GetCloud());

                pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgbCallback(cloudTopic.GetCloud());
                viewerRawCloud->updatePointCloud<pcl::PointXYZRGB>(cloudTopic.GetCloud(), rgbCallback, "sample cloud");

                pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgbSegmented(colourizedSegmentedCloud);
                viewerSegPlanes->updatePointCloud<pcl::PointXYZRGB>(colourizedSegmentedCloud, rgbSegmented, "sample cloud");
            }

            viewerRawCloud->spinOnce(20);
            viewerSegPlanes->spinOnce(20);

            if (viewerRawCloud->wasStopped() || viewerSegPlanes->wasStopped())
                break;
        }

        if (imageTopicRGB.IsNewImageReceived())
        {
            imageTopicRGB.Display();
            rosRGBPictureRecorder->SaveTask(imageTopicRGB.GetImage());
        }

        if (imageTopicDepth.IsNewImageReceived())
        {
            imageTopicDepth.DisplayHistEqualized(1 / 20.0);
            rosDepthPictureRecorder->SaveTask(imageTopicDepth.GetImage());
        }

        if (cv::waitKey(10) >= 0)
            break;
        ros::spinOnce();
    }

    std::cout << std::endl;

    camPictureRecorder->Close();
    rosRGBPictureRecorder->Close();
    rosDepthPictureRecorder->Close();

    return EXIT_SUCCESS;
}

void 
Invitation(void)
{

    std::cout << std::endl;
    std::cout << "Welcome to RosOpencvViewer app." << std::endl;
    std::cout << "You can use this app to as a starting point of your own app." << std::endl;
    std::cout << "Detected OpenCV version: " << CV_VERSION << std::endl;
    std::cout << std::endl;
    std::cout << "PCL VERSION: " << PCL_VERSION << std::endl;
}

int 
ConnectToCamera(int cameraID, cv::Size *frameSize, cv::VideoCapture *cap)
{
    *cap = cv::VideoCapture(cameraID);
    if (!cap->isOpened())
    {
        std::cout << "VideoCapture error." << std::endl;
        return 0;
    }
    else
    {
        cap->set(cv::CAP_PROP_FRAME_WIDTH, frameSize->width);
        cap->set(cv::CAP_PROP_FRAME_HEIGHT, frameSize->height);
        frameSize->width = cap->get(cv::CAP_PROP_FRAME_WIDTH);
        frameSize->height = cap->get(cv::CAP_PROP_FRAME_HEIGHT);
        std::cout << "VideoCapture opened!" << std::endl;
        std::cout << "Size: " << frameSize->width << "x" << frameSize->height << std::endl;
    }
    return 1;
}

boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>>
SegmentPlanes(pcl::SACSegmentation<pcl::PointXYZRGB> &seg, pcl::PointCloud<pcl::PointXYZRGB> &cloud)
{
    typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloudToBeSegmented(new pcl::PointCloud<pcl::PointXYZRGB>);
    typename boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> colourizedSegmentedCloud(new pcl::PointCloud<pcl::PointXYZRGB>);

    pcl::copyPointCloud(cloud, *cloudToBeSegmented);

    PrintSegmentInfo<pcl::PointXYZRGB>(cloudToBeSegmented, false);

    int i = 0;
    int nr_points = (int)cloudToBeSegmented->points.size();
    while (cloudToBeSegmented->points.size() > 0.2 * nr_points && i <= 7)
    {
        i++;

        boost::shared_ptr<pcl::ModelCoefficients> coefficients(new pcl::ModelCoefficients);
        boost::shared_ptr<pcl::PointIndices> inliers(new pcl::PointIndices);
        seg.setInputCloud(cloudToBeSegmented);
        seg.segment(*inliers, *coefficients);

        if (inliers->indices.size() == 0)
        {
            break;
        }

        // Extract the planar inliers from the input cloud
        pcl::ExtractIndices<pcl::PointXYZRGB> extract;
        extract.setInputCloud(cloudToBeSegmented);
        extract.setIndices(inliers);
        extract.setNegative(false);

        // Get the points associated with the planar surface
        boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloudPlane(new pcl::PointCloud<pcl::PointXYZRGB>);
        cloudPlane.get()->clear();
        extract.filter(*cloudPlane);

        // Remove the planar inliers, extract the rest
        extract.setNegative(true);
        extract.filter(*cloudToBeSegmented);

        for (int j = 0; j < cloudPlane.get()->points.size(); j++)
        {
            cloudPlane.get()->points.at(j).rgba = ImgProc::GetEasyDistinguishableColor(i).rgba;
        }

        *colourizedSegmentedCloud.get() += *cloudPlane.get();
    }

    return colourizedSegmentedCloud;
}


template <typename PointT>
int
PrintSegmentInfo(const typename boost::shared_ptr<const pcl::PointCloud<PointT>> cloud, bool computeDensity)
{
    std::cout << "------------------------------------------------------" << std::endl;

    if(computeDensity)
    {
        std::cout << "Cloud resolution: " << ImgProc::ComputeCloudResolution<PointT>(cloud) << std::endl;
    }
    
    std::cout << "temp_cloud_segm: " << cloud.get()->size() << std::endl;
    std::cout << cloud.get()->width << ", isdense = " << (cloud.get()->is_dense ? "true" : "false") << std::endl;
}
template int PrintSegmentInfo<pcl::PointXYZRGB>(const typename boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGB>> cloud, bool computeDensity);
template int PrintSegmentInfo<pcl::PointXYZ>(const typename boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> cloud, bool computeDensity);

